package com.example.student.snapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.backendless.Backendless;

public class MainActivity extends AppCompatActivity {

    public static final String APP_ID = "0D5E76D0-08E5-92D9-FFF6-606DF63AE400";
    public static final String SECRET_KEY = "FFADBAC3-4E3C-775C-FF57-888F4A13A300";
    public static final String VERSION = "v1";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        Backendless.initApp(this, APP_ID, SECRET_KEY, VERSION);
        if (Backendless.UserService.loggedInUser() == "") {
            MenuFragment menu = new MenuFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container,menu).commit();
        }else {
            MainMenuFragment mainMenu = new MainMenuFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container,mainMenu).commit();

        }
    }

    public void showMessage(View v){
        Toast.makeText(getApplicationContext(), "Hello", Toast.LENGTH_SHORT).show();
    }
}
