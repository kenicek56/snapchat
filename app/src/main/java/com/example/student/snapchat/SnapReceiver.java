package com.example.student.snapchat;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class SnapReceiver extends BroadcastReceiver {
    public SnapReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
  String action = intent.getAction();
        if (action.equals(Constants.BROADCAST_ADD_FRIEND_SUCCESS)) {
            Toast.makeText(context, "added friend", Toast.LENGTH_SHORT).show();
        }else if (action.equals(Constants.BROADCAST_ADD_FRIEND_FAILURE)){
            Toast.makeText(context, "failed to add friend", Toast.LENGTH_SHORT).show();

        }else if (action.equals(Constants.BROADCAST_FRIEND_REQUEST_SUCCESS)) {
            Toast.makeText(context, "friend request sent", Toast.LENGTH_SHORT).show();

        }else if (action.equals(Constants.BROADCAST_ADD_FRIEND_FAILURE)) {
            Toast.makeText(context, "failed to send friend request", Toast.LENGTH_SHORT).show();

        }
    }
}
