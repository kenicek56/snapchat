package com.example.student.snapchat;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

import java.util.ArrayList;


public class FriendsFragment extends Fragment {

    private ArrayList<String> friends;
    private ArrayAdapter<String> friendListAdapter;

    public FriendsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_friends, container, false);

        final Uri imageToSend = getActivity().getIntent().getParcelableExtra("ImageURI");

        friends = new ArrayList<String>();
        friendListAdapter = new ArrayAdapter<String>(
                getActivity(), android.R.layout.simple_list_item_1, friends);

        final ListView friendList = (ListView) view.findViewById(R.id.friendList);
        friendList.setAdapter(friendListAdapter);

        final String currentUser = Backendless.UserService.loggedInUser();
        Backendless.Persistence.of(BackendlessUser.class).findById(currentUser, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser user) {
                Object[] friendObjects = (Object[]) user.getProperty("friends");
                if(friendObjects.length > 0) {
                    BackendlessUser[] friendArray = (BackendlessUser[]) friendObjects;
                    for (BackendlessUser friend : friendArray) {
                        String name = friend.getProperty("name").toString();
                        friends.add(name);
                        friendListAdapter.notifyDataSetChanged();
                    }
                }

                final String currentUserName = (String) user.getProperty("name");
                friendList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String friendName = (String) parent.getItemAtPosition(position);
                        sendImageToFriend(currentUserName, friendName, imageToSend);
                    }
                });
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.e("FriendsFrag", fault.toString());
            }
        });

        return view;

    }

    private  void sendImageToFriend(String currentUser, String toUser, Uri imageURI) {
        Intent intent = new Intent(getActivity(), SnapService.class);
        intent.setAction(Constants.ACTION_SEND_PHOTO);
        intent.putExtra("fromUser", currentUser);
        intent.putExtra("toUser", toUser);
        intent.putExtra("imageUri", imageURI);
        Log.i("FFrag", "Uri: " + imageURI.toString());
       // getActivity().startService(intent);
    }
}
