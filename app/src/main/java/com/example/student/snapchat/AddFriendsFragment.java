package com.example.student.snapchat;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;


public class AddFriendsFragment extends Fragment {

    public AddFriendsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_friends, container, false);

        Button addFriendButton = (Button) view.findViewById(R.id.addFriendButton);
        addFriendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setMessage("Add a friend");

                final EditText inputField = new EditText(getActivity());
                alertDialog.setView(inputField);

                alertDialog.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // the user cancels do something
                    }
                });

                alertDialog.setPositiveButton("add friend", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sendFriendRequest(inputField.getText().toString());

                    }
                });

                alertDialog.create();
                alertDialog.show();
            }
        });
        return view;
    }

    private void sendFriendRequest(final String friendName) {
        String currentUserId = Backendless.UserService.loggedInUser();
        Backendless.Persistence.of(BackendlessUser.class).findById(currentUserId, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser currUser) {
                Log.i("addingfriendfragment", "sendfriendrequest");
                Intent intent = new Intent(getActivity(), SnapService.class);
                intent.setAction(Constants.ACTION_SEND_FRIEND_REQUEST);
                intent.putExtra("fromUser", currUser.getProperty("name").toString());
                intent.putExtra("toUser", friendName);

                getActivity().startService(intent);
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.e("addfriendfrag", fault.toString());

            }
        });

    }

}
